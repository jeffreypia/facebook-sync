<?php
/**
 * Plugin Name: Facebook Sync
 * Description: Imports Facebook status updates into WordPress as custom posts
 * Author: Jeffrey Pia
 * Author URI: http://jeffreypia.com
 * Version: 0.1
 * License: MIT
 */

/* ==========================================================================
		REQUIRES
	 ========================================================================== */

// base path
$base = plugin_dir_path( __FILE__ ) . "inc";

require_once("$base/vars.php");

// Register post type
require_once("$base/post-type/ps-facebook.php");

// Create admin page for Facebook settings
require_once("$base/admin.php");

// Reference FB SDK 
require_once("$base/vendor/facebook.php");

// Create custom interval for cron job
add_filter( 'cron_schedules', 'fb_sync_add_scheduled_interval' );
function fb_sync_add_scheduled_interval($schedules) {
	$schedules['fb_sync_15_mins'] = array(
		'interval' => 15*60,
		'display' => '15 minutes',
	);
	return $schedules;
}

// On plugin activation, clear any leftover cron jobs and schedule new cron job
register_activation_hook( __FILE__, 'fb_sync_plugin_activation' );
function fb_sync_plugin_activation() {
	wp_clear_scheduled_hook( 'sync_facebook' );
	wp_schedule_event( time(), 'fb_sync_15_mins', 'sync_facebook' );
}

// On deactivation, clear cron jobs
register_deactivation_hook( __FILE__, 'fb_sync_plugin_deactivation');
function fb_sync_plugin_deactivation() {
	wp_clear_scheduled_hook( 'sync_facebook' );
}

// Cron job
// Check DB for most recent post's date
// Query FB for all updates after that date
// Add create new custom post for each new update
add_action( 'sync_facebook', 'fb_sync_start_post_sync' );
function fb_sync_start_post_sync() {
	global $wpdb;
	
	$fb = fb_sync_get_fb_api();

	// Query FB for posts from last recorded post date
	$add_seconds_server = date('Z');
	$lastPostFB = $wpdb->get_var("SELECT DATE_ADD(post_date_gmt, INTERVAL '$add_seconds_server' SECOND) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'ps_facebook' ORDER BY post_date_gmt DESC LIMIT 1");

	$path = '/'.FB_PAGE.'/posts';
	$path = $lastPostFB ? $path.'?since='.strtotime ( $lastPostFB ) : $path;
	$response = $fb->api(
	 $path
	);

	// foreach through assets and insert to WordPress
	$added_post_ids = array();
	foreach ( $response['data'] as $postSocial ) {
		$added_post_ids[] = fb_sync_add_post( $postSocial );
	}
	return $added_post_ids;
}

function fb_sync_add_post( $postSocial ) {
	global $wpdb;

	$postSocialID = sanitize_text_field( $postSocial['id'] );

	// Check if FB post ID exists in WP
	$postID = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON post_id = ID WHERE post_type = 'ps_facebook' AND meta_key = 'fb_sync_id' AND meta_value = %s LIMIT 1", $postSocialID ) );

	// If not, create new WP post
	if ( ! $postID ) {
		$cleanMessage = sanitize_text_field( $postSocial['message'] );
		$cleanDate = sanitize_text_field( $postSocial['created_time'] );

		$date = new DateTime($cleanDate);
		$utc_date = $date->format('Y-m-d H:i:s');

		$date->setTimeZone(new DateTimeZone('America/New_York'));
		$pub_date = $date->format('Y-m-d H:i:s');

		// If FB post is empty, do not create new post
		if ( ! $cleanMessage ) {
			return false;
		}

		$newPost = array(
			'post_title'      => $cleanMessage,
			'post_content'    => $cleanMessage,
			'post_status'     => 'publish',
			'ping_status'     => 'closed',
			'comment_status'  => 'closed',
			'post_type'       => 'ps_facebook',
			'post_date'       => $pub_date,
			'post_date_gmt'   => $utc_date,
			'post_author'     => 1
		);

		$postID = wp_insert_post( $newPost, true );

		if ( ! $postID  ) {
			return false;
		}

		update_post_meta( $postID, 'fb_raw', serialize($postSocial) );
		update_post_meta( $postID, 'fb_sync_id', $postSocialID );
		update_post_meta( $postID, 'fb_sync_source', 'Facebook' );
		if(isset($postSocial['picture']))       update_post_meta( $postID, 'fb_sync_picture', esc_url_raw( $postSocial['picture'] ) );
		if(isset($postSocial['link']))          update_post_meta( $postID, 'fb_sync_link', esc_url_raw( $postSocial['link'] ) );
		if(isset($postSocial['type']))          update_post_meta( $postID, 'fb_sync_type', sanitize_text_field( $postSocial['type'] ) );
		if(isset($postSocial['status_type']))   update_post_meta( $postID, 'fb_sync_status_type', sanitize_text_field( $postSocial['status_type'] ) );
		if(isset($postSocial['created_time']))  update_post_meta( $postID, 'fb_sync_created_time', $cleanDate );
		if(isset($postSocial['updated_time']))  update_post_meta( $postID, 'fb_sync_updated_time', sanitize_text_field( $postSocial['updated_time'] ) );
		if(isset($postSocial['name']))          update_post_meta( $postID, 'fb_sync_video_name', sanitize_text_field( $postSocial['name'] ) );
		if(isset($postSocial['source']))        update_post_meta( $postID, 'fb_sync_video_source', esc_url_raw( $postSocial['source'] ) );
		if(isset($postSocial['caption']))       update_post_meta( $postID, 'fb_sync_video_caption', sanitize_text_field( $postSocial['caption'] ) );
	}

	return $postID;
}

function fb_sync_get_fb_api() {
	static $facebook;

	$config = array(
		'appId' => APP_ID, // App ID from above
		'secret' => SECRET, // Secret from above
		'fileUpload' => false, // optional
		'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
	);

	if ( ! isset( $facebook ) )
		$facebook = new Facebook($config); // Generates FB object

	return $facebook;
}

// On every page load, check if cron job has been scheduled.
add_action( 'init', 'fb_sync_admin_init' );
function fb_sync_admin_init() {
	// Schedule an action if it's not already scheduled
	if ( ! wp_next_scheduled( 'sync_facebook' ) ) {
		wp_schedule_event( time(), 'fb_sync_15_mins', 'sync_facebook' );
	}
}
