<?php
class FacebookSyncPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_facebook_sync_page' ) );
        add_action( 'admin_init', array( $this, 'facebook_sync_init' ) );
    }

    /**
     * Add options page
     */
    public function add_facebook_sync_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Post Sync Options: Facebook', 
            'Post Sync Options: Facebook', 
            'manage_options', 
            'facebook-sync-admin', 
            array( $this, 'create_facebook_sync_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_facebook_sync_page()
    {
        // Set class property
        $this->options = get_option( 'facebook_sync_option' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Facebook Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'facebook_sync_options' );   
                do_settings_sections( 'facebook-sync-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function facebook_sync_init()
    {        
        register_setting(
            'facebook_sync_options',
            'facebook_sync_option',
            array( $this, 'sanitize' )
        );

        add_settings_section(
            'facebook_sync_options_section',
            'My Custom Settings',
            array( $this, 'print_section_info' ),
            'facebook-sync-admin'
        );  

        add_settings_field(
            'app_id', 
            'App ID', 
            array( $this, 'app_id_callback' ), 
            'facebook-sync-admin', 
            'facebook_sync_options_section'
        );      

        add_settings_field(
            'secret',
            'Secret', 
            array( $this, 'secret_callback' ),
            'facebook-sync-admin',
            'facebook_sync_options_section'
        );      

        add_settings_field(
            'fb_page',
            'FB Page ID or Name', 
            array( $this, 'fb_page_callback' ),
            'facebook-sync-admin',
            'facebook_sync_options_section'
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['secret'] ) )
            $new_input['secret'] = sanitize_text_field( $input['secret'] );

        if( isset( $input['app_id'] ) )
            $new_input['app_id'] = sanitize_text_field( $input['app_id'] );

        if( isset( $input['fb_page'] ) )
            $new_input['fb_page'] = sanitize_text_field( $input['fb_page'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your App info below:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function secret_callback()
    {
        printf(
            '<input type="text" id="secret" name="facebook_sync_option[secret]" value="%s" />',
            isset( $this->options['secret'] ) ? esc_attr( $this->options['secret']) : ''
        );
    }

    public function app_id_callback()
    {
        printf(
            '<input type="text" id="app_id" name="facebook_sync_option[app_id]" value="%s" />',
            isset( $this->options['app_id'] ) ? esc_attr( $this->options['app_id']) : ''
        );
    }

    public function fb_page_callback()
    {
        printf(
            '<input type="text" id="fb_page" name="facebook_sync_option[fb_page]" value="%s" />',
            isset( $this->options['fb_page'] ) ? esc_attr( $this->options['fb_page']) : ''
        );
    }
}

if( is_admin() )
    $facebook_sync_page = new FacebookSyncPage();

?>