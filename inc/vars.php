<?php

$options = get_option('facebook_sync_option');

// Define constants for FB app and page info
define( 'APP_ID',   $options['app_id'] );
define( 'SECRET',   $options['secret'] );
define( 'FB_PAGE',  $options['fb_page'] );

?>