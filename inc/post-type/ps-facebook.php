<?php

add_action( 'init', 'register_cpt_ps_facebook' );
function register_cpt_ps_facebook() {

	$labels = array( 
		'name' => _x( 'Facebook Posts', 'ps_facebook' ),
		'singular_name' => _x( 'Facebook Post', 'ps_facebook' ),
		'add_new' => _x( 'Add New Facebook Post', 'ps_facebook' ),
		'all_items' => _x( 'Facebook Posts', 'ps_facebook' ),
		'add_new_item' => _x( 'Add New Facebook Post', 'ps_facebook' ),
		'edit_item' => _x( 'Edit Facebook Post', 'ps_facebook' ),
		'new_item' => _x( 'New Facebook Post', 'ps_facebook' ),
		'view_item' => _x( 'View Facebook Post', 'ps_facebook' ),
		'search_items' => _x( 'Search Facebook Posts', 'ps_facebook' ),
		'not_found' => _x( 'No Facebook Posts found', 'ps_facebook' ),
		'not_found_in_trash' => _x( 'No Facebook Posts found in Trash', 'ps_facebook' ),
		'parent_item_colon' => _x( 'Parent Facebook Post:', 'ps_facebook' ),
		'menu_name' => _x( 'Facebook', 'ps_facebook' ),
	);

	$supports = array(
		'title',
		'editor',
		'custom-fields'
	);

	$args = array( 
		'labels' => $labels,
		'hierarchical' => true,
		'supports' => $supports,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'exclude_from_search' => true,
		'menu_position' => 11,
		'menu_icon' => 'dashicons-facebook-alt',
	);
	register_post_type( 'ps_facebook', $args );


}
